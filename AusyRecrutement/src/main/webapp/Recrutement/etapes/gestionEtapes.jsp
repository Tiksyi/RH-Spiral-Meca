<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:if test="#session.utilisateur !=null">
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
<head>
<meta http-equiv= "Content-Type" content= "text/html; charset=UTF-8">
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> -->
<title>Gestion des �tapes</title>
<link rel="stylesheet" href="./css/index.css" />
<link rel="stylesheet" href="./css/ajoutCandidat.css">
</head>
<body>
	<%@include file="../pages/header.html"%>
	<h1 class="TitrePage">Gestion des �tapes</h1>

	<div id="content">
		<s:form action="GestionEtape">

			<s:select label="Choix de l'�tape * " name="choixEtape"
				list="lstEtapes" listKey="idEtape" listValue="nomEtape"
				headerKey="0" headerValue=" -- Choisir -- " />
			<s:submit value="Afficher" method="afficher"></s:submit>

			<s:textfield label="Nom *" name="nom" size="60" />
			<s:textarea label="Description" name="description" cols="45" rows="8" />
			<s:textfield label="Type *" name="type" size="60" />
			<s:textfield label="Type d'interlocuteur *" name="typeInterlocuteur"
				size="60" />

			<s:submit value="Modifier" method="execute"></s:submit>
		</s:form>
		<s:if test="#request['erreur']=='aucune'">
						Un e-mail a �t� envoy�.
		</s:if>
		<s:if test="#request['erreur']!=null">
			<p class="CouleurRouge">
				<s:property value="#request['erreur']" />
			</p>
		</s:if>
		<p align="left">
			<s:a href="Accueil">Retour</s:a>
		</p>
	</div>

	<%@include file="../pages/formConnecte.jsp"%>
	<%@include file="../pages/footer.jsp"%>


</body>
	</html>
</s:if>