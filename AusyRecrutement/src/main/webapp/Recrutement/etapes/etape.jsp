<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<s:if test="#session.utilisateur !=null">
	<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="./css/index.css" />
		<link rel="stylesheet" href="./css/etape.css" />
		<title><s:property
				value="#request['etapeCherchee.getNomEtape()']" /></title>
		</head>
		<body>
			<%@include file="../pages/header.html"%>
		
			<h1 class="TitrePage">
				<s:property value="#request['etapeCherchee.getNomEtape()']" />
			</h1>
		
			<div id="content">
				<s:form action="Etape">
					<s:hidden name="param1"></s:hidden>
					<div class="illustration-texte">
						<img id="Entretien"
							src="./images/<s:property value="#request['etapeCherchee.getTypeEtape()']"/>.jpg"
							width="500" height="333" border="0" usemap="#map" alt="Entretien"
							class="illus-gauche" />
						<p>
							<s:property value="#request['etapeCherchee.getDescriptionEtape()']" />
						</p>
					</div>
					<p>
						Interlocuteur :
						<s:property value="#request['etapeCherchee.getTypeInterlocuteur()']" />
						
					</p>
					<s:if test="#request['participationEtapeCherchee']!=null">
						<p>
							<s:property value="#request['participationEtapeCherchee.getInterlocuteur()']" />
						</p>
						<p>
							Date :
							<s:property
								value="#request['participationEtapeCherchee.getDateEtape()']" />
							Heure :
							<s:property
								value="#request['participationEtapeCherchee.getHeureEtape()']" />
						</p>
						<p>
							Lieu :
							<s:property
								value="#request['participationEtapeCherchee.getLieuEtape()']" />
						</p>
						<s:if test="#request.param1 ==6">
							<!-- //Sp�cifique � la 5�me �tape  -->
						</s:if>
					</s:if>
				</s:form>
			</div>
		
		
		
			<%@include file="../pages/formConnecte.jsp"%>
			<%@include file="../pages/footer.jsp"%>
		</body>
	</html>
</s:if>