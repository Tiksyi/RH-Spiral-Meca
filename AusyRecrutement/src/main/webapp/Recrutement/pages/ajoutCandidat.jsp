<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:if test="#session.utilisateur !=null">
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nouveau Candidat</title>

<link rel="stylesheet" href="./css/index.css" />
<link rel="stylesheet" href="./css/ajoutCandidat.css">

</head>
<body>
	<%@include file="header.html"%>
	<h1 class="TitrePage">Ajout d'un nouveau candidat</h1>

	<div id="content">
		<s:form action="/Recrutement/AjoutCandidat" scope="request">
			Un mail est automatiquement envoy� au candidat lors de l'ajout avec son login et son mot de passe.	 
			<s:select label="Civilit�" name="civilite" list="{'M.','Mme'}"
				value="%{'M.'}" />
			<s:textfield label="Nom *" name="nom" value="" />
			<s:textfield label="Pr�nom *" name="prenom" value="" />
			<%-- <s:textfield label="T�l�phone" name="telephone" value="" /> --%>
			<s:textfield label="Mail *" name="mail" value="" />
			<s:checkbox label="Administrateur" value="false" name="admin"></s:checkbox>
			<s:submit value="Ajouter"></s:submit>
		</s:form>

		<s:if test="#request['erreur']=='aucune'">
			<p>Un e-mail a �t� envoy�.</p>

		</s:if>
		<s:if test="#request['erreur']!=null">
			<p class="CouleurRouge">
				<s:property value="#request['erreur']" />
			</p>
		</s:if>

		<s:if test="#request['identifiants']!=null">
			<p>
				<s:property value="#request['identifiants']" />
			</p>
		</s:if>
		<p align="left">
			<s:a href="Accueil">Retour</s:a>
		</p>
	</div>

	<%@include file="formConnecte.jsp"%>


	<%@include file="footer.jsp"%>


</body>
	</html>
</s:if>