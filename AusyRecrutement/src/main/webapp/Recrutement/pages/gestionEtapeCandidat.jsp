<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<s:if test="#session.utilisateur !=null">
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Gestion des �tapes des candidats</title>
<sx:head cache="false" compressed="false" />
<!-- pour afficher le datapicker -->
<link rel="stylesheet" href="./css/index.css" />
<link rel="stylesheet" href="./css/ajoutCandidat.css">
</head>
<body>
	<%@include file="header.html"%>
	<h1 class="TitrePage">Gestion des �tapes des candidats</h1>

	<div id="content">
		<s:form action="GestionEtapeCandidat">
			<s:select label="Choix du candidat * " name="choixCandidat"
				list="lstCandidats" listKey="id" listValue="nom + ' ' + prenom"
				headerKey="0" headerValue=" -- Choisir -- " value="" />

			<s:select label="Choix de l'�tape * " name="choixEtape"
				list="lstEtapes" listKey="idEtape" listValue="nomEtape"
				headerKey="0" headerValue=" -- Choisir -- " value="" />

			<sx:datetimepicker name="dateEtape" label="Date (dd/MM/yyyy) * "
				displayFormat="dd/MM/yyyy" value=""></sx:datetimepicker>

			<sx:datetimepicker name="heureEtape" label="Heure (--:--) * "
				type="time" displayFormat="HH:mm" value=""></sx:datetimepicker>

			<s:textfield label="Lieu " name="lieu" value="" />
			<s:textfield label="Interlocuteur " name="interlocuteur" value="" />
			<%-- <s:checkbox property="booEnvoiMail">Envoyer un mail au candidat</s:checkbox><br/><br/> --%>
			<s:submit value="Ajouter"></s:submit>
		</s:form>
		<s:if test="#request['erreur']=='aucune'">
						Un e-mail a �t� envoy�.
					</s:if>
		<s:if test="#request['erreur']!=null">
			<s:property value="#request['erreur']" />
		</s:if>

		<p align="left">
			<s:a href="Accueil">Retour</s:a>
		</p>
	</div>

	<%@include file="formConnecte.jsp"%>


	<%@include file="footer.jsp"%>


</body>
	</html>
</s:if>