<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@ page import="modele.Compte" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<s:set var="monUtilisateur" value="utilisateur" scope="session"/>

<s:if test="#session.utilisateur !=null">

	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<link rel="stylesheet" href="./css/accueilCandidat.css"><!-- "./css/accueilCandidat.css"> -->
	<link rel="stylesheet" href="./css/index.css">
	
	<title>Bienvenue </title>
	</head>
	
	<%@include file="header.html"%>
	
	<body>
			<img id="Spirale" src="pages/AUSY_RH_v2.2_no_surfeur_HR.jpg" width="1240"
				 height="877" border="0" usemap="#map" />
				
				
	
			<map name="map">
				<!-- #$-:Image map file created by GIMP Image Map plug-in -->
				<!-- #$-:GIMP Image Map plug-in by Maurits Rijk -->
				<!-- #$-:Please do not edit lines starting with "#$" -->
				<!-- #$VERSION:2.3 -->
				<!-- #$AUTHOR:utilisateurAER -->
				<!-- <area shape="rect" coords="259,516,308,765" nohref="nohref" /> -->
				
				<area class=areaSpirale shape="rect" coords="124,536,378,831" alt="Etape 1" target="etape1"
					href="Etape?param1=0" title="<s:property value="#request.lstEtapes.get(0).getNomEtape()" />"/>
				
			 	<area class=areaSpirale shape="poly" coords="432,830,411,759,406,681,414,616,509,650,571,659,640,664,642,664,677,726,712,767,750,800,815,837,803,840,702,854,610,858,525,855,433,843,434,842" 
			 	alt="Etape 2" target="etape2" href="Etape?param1=1" title="<s:property value="#request.lstEtapes.get(1).getNomEtape()" />" />
			 	
				<area class=areaSpirale shape="poly" coords="733,661,779,657,831,646,888,628,910,619,961,642,1031,666,1098,673,1155,672,1135,695,1098,727,1042,763,991,785,936,806,871,825,846,831,841,823,812,812,781,794,740,763,706,729,662,664" 
				alt="Etape 3" target="etape3" href="Etape?param1=2" title="<s:property value="#request.lstEtapes.get(2).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="1006,539,966,583,922,612,991,642,1045,656,1168,657,1195,604,1206,569,1207,522,1196,477,1181,445,1172,468,1148,487,1101,503,1049,510,1021,510" 
				alt="Etape 4" target="etape4" href="Etape?param1=3" title="<s:property value="#request.lstEtapes.get(3).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="1026,434,1030,465,1025,504,1085,498,1130,486,1159,467,1169,450,1172,429,1142,388,1090,341,1026,302,951,266,973,282,992,301,1012,329,1020,353,1017,375,1009,395" 
				alt="Etape 5" target="etape5" href="Etape?param1=4" title="<s:property value="#request.lstEtapes.get(4).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="1006,392,966,348,917,310,845,272,773,243,716,231,678,226,774,228,830,235,887,246,947,266,978,291,996,308,1016,347,1015,372,1006,390" 
				alt="Etape 6" target="etape6" href="Etape?param1=5" title="<s:property value="#request.lstEtapes.get(5).getNomEtape()" />" />
					
				<area class=areaSpirale shape="poly" coords="816,268,772,252,708,235,671,229,602,229,537,241,478,255,402,286,353,319,324,352,304,385,329,380,370,373,394,372,396,368,415,341,445,318,477,300,533,281,588,269,623,264,704,261,758,264,791,268,824,276,855,288" 
				alt="Etape 7" target="etape7" href="Etape?param1=6" title="<s:property value="#request.lstEtapes.get(6).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="336,381,367,376,391,373,392,377,388,400,389,424,398,452,413,474,432,495,475,522,526,541,592,551,657,552,717,547,773,536,782,560,807,582,826,593,789,602,715,612,642,616,571,613,504,601,450,586,395,563,344,524,317,490,303,460,297,439,296,410,303,386" 
				alt="Etape 8" target="etape8" href="Etape?param1=7" title="<s:property value="#request.lstEtapes.get(7).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="786,558,802,574,830,591,884,573,925,551,960,522,979,497,991,469,993,439,988,412,954,416,927,418,901,412,891,411,893,433,890,449,877,475,852,500,821,518,778,535" 
				alt="Etape 9" target="etape9" href="Etape?param1=8" title="<s:property value="#request.lstEtapes.get(8).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="950,414,928,414,901,409,890,408,881,380,862,358,831,333,802,319,767,308,739,304,729,302,729,291,723,279,714,267,706,264,704,262,728,264,759,266,776,268,812,275,832,281,850,287,905,313,943,345,971,375,985,403,987,408"
				alt="Etape 10" target="etape10" href="Etape?param1=9" title="<s:property value="#request.lstEtapes.get(9).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="714,302,680,302,657,304,640,306,613,309,595,313,569,322,547,331,529,344,517,354,508,366,504,380,504,398,504,421,507,425,510,434,503,436,494,436,484,439,475,444,467,445,450,454,437,462,423,472,419,474,403,452,394,423,391,404,394,386,399,369,411,351,422,338,443,322,472,305,507,292,577,273,638,265,689,263,703,263,713,270,720,278,725,289,727,303,726,304,726,302" 
				alt="Etape 11" target="etape11" href="Etape?param1=10" title="<s:property value="#request.lstEtapes.get(10).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="446,461,463,451,489,443,509,437,514,437,523,451,539,463,564,475,602,484,643,487,684,485,718,477,745,466,766,452,782,437,799,450,819,457,846,464,867,463,879,462,866,481,847,497,825,510,805,520,781,528,759,535,726,542,694,545,664,548,601,547,570,545,534,537,495,526,461,509,440,495,429,485,424,476" 
				alt="Etape 12" target="etape12" href="Etape?param1=11" title="<s:property value="#request.lstEtapes.get(11).getNomEtape()" />" />
				
				<area class=areaSpirale shape="poly" coords="800,446,818,454,849,461,872,460,881,457,886,447,889,418,878,383,853,353,826,333,797,320,771,312,740,305,708,304,686,303,621,309,618,311,630,312,650,315,659,319,670,326,674,335,677,341,703,339,722,341,744,345,769,355,788,371,798,388,800,402,799,414,787,433" 
				alt="Etape 13" target="etape13" href="Etape?param1=12" title="<s:property value="#request.lstEtapes.get(12).getNomEtape()" />" />

				<area class=areaSpirale shape="poly" coords="667,329,647,316,624,313,610,312,579,319,559,328,534,342,518,356,511,373,507,394,508,417,514,433,527,451,551,465,586,478,624,483,662,484,691,480,720,472,741,463,763,450,780,435,785,426,772,433,750,441,722,445,689,443,654,437,629,424,611,406,608,389,610,376,625,359,642,350,672,341" 
				alt="Etape 14" target="etape14" href="Etape?param1=13" title="<s:property value="#request.lstEtapes.get(13).getNomEtape()" />" />
			</map>
	
	
		<div id="Paragr">
	
			<h2>F�licitations!</h2>
			Vous �tes � l'�tape N Ou Vous etes collaborateur d'Ausy
			<!-- Si c'est un admin, il faut pouvoir modifier des �tapes etc... -->
			<s:if test="#session.utilisateur.getStatut().getStatut()=='Administrateur'">
				<p align="center">

					<s:a href="AjoutCandidat">Ajouter un nouveau candidat</s:a>
				</p>
				<p align="center">
					<s:a href="GestionEtape">Les �tapes</s:a>
				</p>
				<p align="center">
					<s:a href="GestionEtapeCandidat">Ajouter les �tapes � un candidat</s:a>
				</p>
				
			</s:if>
			<br />
		</div>
	
		<%@include file="formConnecte.jsp"%>
	
	
	
		<%@include file="footer.jsp"%>
		
		
	
	
	</body>
	</html>
 </s:if>