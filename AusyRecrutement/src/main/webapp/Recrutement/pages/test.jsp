<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head><title>Struts 2  Hibernate 3 Integration</title>
</head>
<body>
<h1>Struts 2  Hibernate 3 Integration</h1>
<s:form action="result" validate="true" >
	<s:textfield name="name" label="Name"/>
	<s:textfield name="email" label="Email"/>
	<s:textfield name="age" label ="Age"/>
	<s:submit/>
</s:form>
</body>
</html> 