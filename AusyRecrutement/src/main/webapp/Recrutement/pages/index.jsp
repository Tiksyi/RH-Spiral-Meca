<%@page import="dao.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Identification</title>

<!-- <link rel="stylesheet" href="./css/index.css" /> -->

<link rel="stylesheet" href="./css/index.css" />
</head>
<body>


	<%@include file="header.html"%>



	<div id="content">


		<h1>Bienvenue sur le portail de candidature AUSY</h1>
		<p> Bienvenue sur le Portail de candidature AUSY r�serv�
		uniquement aux candidats du Groupe, ayant d�j� �t� contact� pour un
		premier entretien. N'h�sitez pas � nous faire part de vos remarques
		sur support.recrutement@ausy.fr.</p><p> Bonne navigation � Tous.
		<p/>
		<s:if test="%{monErreur!=null}">
			<p class="CouleurRouge">${monErreur}<p/>
		</s:if>
		
	</div>


	<%@include file="formConnexion.jsp"%>

	<%@include file="footer.jsp"%>

</body>
</html>
