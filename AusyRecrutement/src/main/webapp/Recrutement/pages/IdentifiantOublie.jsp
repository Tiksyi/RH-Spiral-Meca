<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Identifiant oubli�</title>

<link rel="stylesheet" href="./css/index.css" />

</head>
<body>
	<%@include file="header.html"%>
	<h1>Identifiant oubli�</h1>

	<div id="content">
		<s:form action="IdentifiantOublie">

			<p>Merci de saisir l'adresse e-mail associ�e � votre compte.
			Vous allez recevoir un mail contenant votre identifiant.</p>
			<s:textfield label="Adresse email  " name="mail" size="100" />
			<s:submit value="Envoyer"></s:submit>
		</s:form>

		<s:if test="#request['erreur']==null">
			<s:if test="#request['mail']!=null">
					<p>Un e-mail a �t� envoy� � cette adresse : <s:property
					value="#request['mail']" /></p>
			</s:if>
		</s:if>

		<s:if test="#request['erreur']!=null">
			<p class="CouleurRouge"><s:property value="#request['erreur']" /></p>
		</s:if>
	</div>

	<%@include file="formConnexion.jsp"%>


	<%@include file="footer.jsp"%>


</body>
</html>