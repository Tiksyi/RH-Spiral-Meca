<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" href="../css/menu.css" />

</head>
<body>

	<ul id="menu-deroulant">
		<li><a href="#">Liens utiles</a>
			<ul>
				<li><a href="https://intra.ausy.fr/">Intranet Ausy</a></li>
				<li><a href="http://www.ausy.com/fr">AUSY</a></li>
				<li><a href="http://ceausy.fr/sommaire/auth_user_html.php">CE AUSY</a></li>
				<li><a href="https://webmail.ausy-group.com/zimbra/#1">WEBMAIL AUSY</a></li>
			</ul></li>
		<!--
 -->
		<li><a href="#">Lien menu 2</a>
			<ul>
				<li><a href="#">Lien sous menu 2</a></li>
				<li><a href="#">Lien sous menu 2</a></li>
				<li><a href="#">Lien sous menu 2</a></li>
				<li><a href="#">Lien sous menu 2</a></li>
			</ul></li>
	</ul>

</body>
</html>