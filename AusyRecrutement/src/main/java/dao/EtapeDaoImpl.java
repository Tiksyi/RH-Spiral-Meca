package dao;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import core.DataBaseAccessException;
import core.HibernateUtil;
import modele.Etape;



public class EtapeDaoImpl implements EtapeDao {

	Transaction tx = null;

	/**
	 * a string that send a message error.
	 */
	private static final String ID_NOT_FOUND = "id not found";

	/**
	 * {@inheritDoc}
	 */
	public final boolean create( Etape objEtape) throws DataBaseAccessException {
		Session session = null;
		boolean result = false;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			
			//session.update(objCompte.getStatut());
			//session.update(objCompte);
			
			tx = session.beginTransaction();
			session.save(objEtape);
			tx.commit();
			result = true;
		} catch (HibernateException e) {
			if (session != null && tx != null) {
				tx.rollback();
			}
			result = false;
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	 public final Etape findById(final Long idEtape) throws DataBaseAccessException {
		 Session session = null;
		 Etape result = new Etape();
		 try 
		 {
			 session = HibernateUtil.getSessionFactory().openSession();
			 tx=session.beginTransaction();
			 result = (Etape) session.createCriteria(Etape.class)
			 .add(Restrictions.eq("idEtape", idEtape)).uniqueResult();
			 tx.commit();
		 } 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 throw new DataBaseAccessException(ID_NOT_FOUND);
		 } 
		 finally 
		 {
			 session.close();
		 }
		 return result;
	 }

	
	

	@SuppressWarnings("unchecked")
	public final List<Etape> findAll() throws DataBaseAccessException 
	{
		Session session = null;
		List<Etape> result = new ArrayList<Etape>();
		//Criterion critere=Restrictions.eq("Statut", new Statut(2, "Candidat"));
		
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Etape.class);
		//criteria.add(critere);

		result = (List<Etape>) criteria.list();
		session.getTransaction().commit();
		
		return result;
	}


	/**
	 * {@inheritDoc}
	 */
	 public final boolean update(final Etape etape)
	 throws DataBaseAccessException 
	 {
		 Session session = null;
		 boolean result = false;
		 try 
		 {
			 
					if(etape.getNomEtape()!=null)
					{
						Charset charset = Charset.forName("UTF-8");
						CharsetDecoder decoder = charset.newDecoder();
						CharsetEncoder encoder = charset.newEncoder();
						
						try {
						    // Convert a string to ISO-LATIN-1 bytes in a ByteBuffer
						    // The new ByteBuffer is ready to be read.
						    ByteBuffer bbuf = encoder.encode(CharBuffer.wrap(etape.getNomEtape()));

						    // Convert ISO-LATIN-1 bytes in a ByteBuffer to a character ByteBuffer and then to a string.
						    // The new ByteBuffer is ready to be read.
						    CharBuffer cbuf = decoder.decode(bbuf);
						    String s = cbuf.toString();
						    etape.setNomEtape(s);
						} catch (CharacterCodingException e) {
						}
					}
						
						
						
			 session = HibernateUtil.getSessionFactory().openSession();
			 session.beginTransaction();
			 session.update(etape);
			 session.getTransaction().commit();
			 result = true;
		 } 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 result = false;
		 } 
		 finally 
		 {
			 session.close();
		 }
		 return result;
	 }

	/**
	 * {@inheritDoc}
	 */
	 public final boolean delete(final Etape etape)
	 throws DataBaseAccessException 
	 {
		 Session session = null;
		 boolean result = false;
		 try 
		 {
			 session = HibernateUtil.getSessionFactory().openSession();
			 Transaction tx = session.beginTransaction();
			 session.delete(etape);
			 tx.commit();
			 result = true;
		 } 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 result = false;
		 } 
		 finally 
		 {
			 session.close();
		 }
		 return result;
	 }
}
