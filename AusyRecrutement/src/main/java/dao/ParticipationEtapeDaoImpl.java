package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import core.DataBaseAccessException;
import core.HibernateUtil;
import modele.Compte;
import modele.Etape;
import modele.ParticipationEtape;;

public class ParticipationEtapeDaoImpl implements ParticipationEtapeDao {

	Transaction tx = null;

	/**
	 * {@inheritDoc}
	 */
	public final boolean create(ParticipationEtape objPart) throws DataBaseAccessException {
		Session session = null;
		boolean result = false;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			tx = session.beginTransaction();
			session.save(objPart);
			tx.commit();
			result = true;
		} catch (HibernateException e) {
			if (session != null && tx != null) {
				tx.rollback();
			}

			result = false;
			// if (e.toString().contains("ConstraintViolationException")) {
			// throw new
			// DataBaseAccessException(Constants.erreur_etape_candidat_exist);
			// }
		} finally {
			session.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public final ParticipationEtape findParticipationEtape(Compte candidat, Etape etape)
			throws DataBaseAccessException {
		Session session = null;
		List<ParticipationEtape> result = new ArrayList<ParticipationEtape>();

		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		result = session.createCriteria(ParticipationEtape.class).add(Restrictions.like("candidat", candidat))
				.add(Restrictions.like("etape", etape)).list();

		session.getTransaction().commit();

		ParticipationEtape ObjParticipationTrouve = null;
		if (result.size() > 0) {
			ObjParticipationTrouve = result.get(0);
		}
		return ObjParticipationTrouve;

	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	public final List<ParticipationEtape> findAll(Compte candidat) throws DataBaseAccessException {

		Session session = null;
		List<ParticipationEtape> result = new ArrayList<ParticipationEtape>();
		Criterion critere = Restrictions.eq("candidat.id", candidat.getId());

		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Compte.class).createAlias("candidat", "candidat");
		criteria.add(critere);

		result = (List<ParticipationEtape>) criteria.list();
		session.getTransaction().commit();

		return result;

	}

	/**
	 * {@inheritDoc}
	 */
	public final boolean update(final ParticipationEtape part) throws DataBaseAccessException {
		Session session = null;
		boolean result = false;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.update(part);
			session.getTransaction().commit();
			result = true;
		} catch (HibernateException e) {
			if (session != null && session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			result = false;
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public final boolean delete(final ParticipationEtape part) throws DataBaseAccessException {
		Session session = null;
		boolean result = false;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			session.delete(part);
			tx.commit();
			result = true;
		} catch (HibernateException e) {
			if (session != null && session.getTransaction() != null) {
				session.getTransaction().rollback();
			}
			result = false;
		} finally {
			session.close();
		}
		return result;
	}

}
