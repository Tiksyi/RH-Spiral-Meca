package dao;

import java.util.List;

import core.DataBaseAccessException;
import modele.Etape;

public interface EtapeDao {

	/**
	 * a method that creates a Etape in Etape.
	 * @param Etape the Etape to create
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean create(Etape etape) throws DataBaseAccessException;
	/**
	 * a method that finds a Etape.
	 * @param id the technical id of the searched Etape
	 * @return the searched Etape
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	Etape findById(final Long id) throws DataBaseAccessException;
	
	/**
	 * a method that returns a Etape list.
	 * @return myComptes a list of Etape
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	List<Etape> findAll() throws DataBaseAccessException;
	/**
	 * a method that updates a Etape.
	 * @param Etape my Etape
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean update(Etape etape) throws DataBaseAccessException;
	/**
	 * a method that deletes a Etape.
	 * @param Etape the Etape to delete
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean delete(Etape etape) throws DataBaseAccessException;
	
	
}
