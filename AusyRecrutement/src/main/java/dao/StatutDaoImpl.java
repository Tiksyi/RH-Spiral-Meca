package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import core.DataBaseAccessException;
import core.HibernateUtil;
import modele.Statut;


public class StatutDaoImpl implements StatutDao {

	Transaction tx = null;

	/**
	 * a string that send a message error.
	 */
	private static final String ID_NOT_FOUND = "id not found";


	/**
	 * {@inheritDoc}
	 */
	 public final Statut findById(final Long id) throws
	 DataBaseAccessException 
	 {
		 Session session = null;
		 Statut result = new Statut();
		 try 
		 {
			 session = HibernateUtil.getSessionFactory().openSession();
			 tx=session.beginTransaction();
			 result = (Statut) session.createCriteria(Statut.class)
			 .add(Restrictions.eq("idStatut", id)).uniqueResult();
			 tx.commit();
		} 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 throw new DataBaseAccessException(ID_NOT_FOUND);
		 } 
		 finally  
		 {
			 session.close();
		 }
		 return result;
	 }

	
	/**
	 * {@inheritDoc}
	 */
	public final List<Statut> findAll() throws DataBaseAccessException {

		
		List<Statut> result = new ArrayList<Statut>();
		Connection conn = null;

		try 
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();

			String url = "jdbc:mysql://localhost:3306/candidature";
			String utilisateur = "root";
			String motDePasse = "";
			conn =DriverManager.getConnection( url, utilisateur, motDePasse );			
			PreparedStatement prepareStatement = conn.prepareStatement("SELECT * FROM utilisateur;");
			
			ResultSet executeQuery = prepareStatement.executeQuery();
			
			while ( executeQuery.next() ) 
			{
		
			}
			
			System.out.println("res"+executeQuery.getString(1));
			
			
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return result;
	}






	
	
}
