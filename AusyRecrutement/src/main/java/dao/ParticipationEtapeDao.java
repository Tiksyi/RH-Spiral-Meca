package dao;

import java.util.List;

import core.DataBaseAccessException;
import modele.Compte;
import modele.Etape;
import modele.ParticipationEtape;

public interface ParticipationEtapeDao {

	/**
	 * a method that creates a ParticipationEtape in ParticipationEtape.
	 * @param ParticipationEtape the ParticipationEtape to create
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean create(ParticipationEtape ParticipationEtape) throws DataBaseAccessException;
	
	/**
	 * a method that returns a ParticipationEtape list.
	 * @return myComptes a list of ParticipationEtape
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	List<ParticipationEtape> findAll(Compte candidat) throws DataBaseAccessException;
	
	/**
	 * a method that returns a ParticipationEtape.
	 * @return myComptes a list of ParticipationEtape
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	ParticipationEtape findParticipationEtape(Compte candidat, Etape etape) throws DataBaseAccessException;
	/**
	 * a method that updates a ParticipationEtape.
	 * @param ParticipationEtape my ParticipationEtape
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean update(ParticipationEtape ParticipationEtape) throws DataBaseAccessException;
	/**
	 * a method that deletes a ParticipationEtape.
	 * @param ParticipationEtape the ParticipationEtape to delete
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean delete(ParticipationEtape ParticipationEtape) throws DataBaseAccessException;
	
	
}
