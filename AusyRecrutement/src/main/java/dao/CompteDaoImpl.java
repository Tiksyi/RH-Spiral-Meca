package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import core.DataBaseAccessException;
import core.HibernateUtil;
import modele.Compte;


public class CompteDaoImpl implements CompteDao {

	Transaction tx = null;

	
	/**
	 * a string that send a message error.
	 */
	private static final String ID_NOT_FOUND = "id not found";

	/**
	 * {@inheritDoc}
	 */
	public final boolean create( Compte objCompte) throws DataBaseAccessException {
		Session session = null;
		boolean result = false;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			
			//session.update(objCompte.getStatut());
			//session.update(objCompte);
			
			tx = session.beginTransaction();
			session.save(objCompte);
			tx.commit();
			result = true;
		} catch (HibernateException e) {
			if (session != null && tx != null) {
				tx.rollback();
			}
			result = false;
		} finally {
			session.close();
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	 public final Compte findById(final long id) throws DataBaseAccessException {
		 Session session = null;
		 Compte result = new Compte();
		 try 
		 {
			 session = HibernateUtil.getSessionFactory().openSession();
			 tx=session.beginTransaction();
			 result = (Compte) session.createCriteria(Compte.class)
			 .add(Restrictions.eq("id",new Long(id))).uniqueResult();
			 tx.commit();
		 } 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 throw new DataBaseAccessException(ID_NOT_FOUND);
		 } 
		 finally 
		 {
			 session.close();
		 }
		 return result;
	 }

	/**
	 * {@inheritDoc}
	 */
	 public Compte findByLoginPwd(final String login, String pwd)
	 throws DataBaseAccessException {
		 Session session = null;
		 Compte result = new Compte();
		 try 
		 {
			 session = HibernateUtil.getSessionFactory().openSession();
			 session.beginTransaction();
			 result = (Compte) session.createCriteria(Compte.class)
			 .add(Restrictions.eq("login", login).ignoreCase())
			 .add(Restrictions.eq("mdp", pwd).ignoreCase()).uniqueResult();
			 session.getTransaction().commit();
		 } 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 throw new DataBaseAccessException(ID_NOT_FOUND + e.getMessage());
		 }
		 return result;
	 }
	 
	 
	 
	 	/**
		 * {@inheritDoc}
		 */
		 public Compte findByMail(final String Mail)
		 throws DataBaseAccessException 
		 {
			 Session session = null;
			 Compte result = new Compte();
			 try 
			 {
				 session = HibernateUtil.getSessionFactory().openSession();
				 session.beginTransaction();
				 result = (Compte) session.createCriteria(Compte.class)
				 .add(Restrictions.eq("email", Mail).ignoreCase())
				 .uniqueResult();
				 session.getTransaction().commit();
			 } 
			 catch (HibernateException e) 
			 {
				 if (session != null && session.getTransaction() != null) 
				 {
					 session.getTransaction().rollback();
				 }
				 throw new DataBaseAccessException(ID_NOT_FOUND);
			 }
			 return result;
		 }

	
	@SuppressWarnings("unchecked")
	public final List<Compte> findAllCandidat() throws DataBaseAccessException 
	{
		Session session = null;
		List<Compte> result = new ArrayList<Compte>();
		Criterion critere=Restrictions.eq("statut.idStatut", 2);
		
		session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		
		Criteria criteria = session.createCriteria(Compte.class).createAlias("statut", "statut");
		criteria.add(critere);

		result = (List<Compte>) criteria.list();
		session.getTransaction().commit();
		
		return result;
	}
	
	
	
	/**
	 * {@inheritDoc}
	 */
	public final List<Compte> findAll() throws DataBaseAccessException {

		List<Compte> result = new ArrayList<Compte>();
		Connection conn = null;

		try 
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();

			String url = "jdbc:mysql://localhost:3306/candidature";
			String utilisateur = "root";
			String motDePasse = "";
			conn =DriverManager.getConnection( url, utilisateur, motDePasse );			
			PreparedStatement prepareStatement = conn.prepareStatement("SELECT * FROM utilisateur;");
			
			ResultSet executeQuery = prepareStatement.executeQuery();
			
			while ( executeQuery.next() ) 
			{
		
			}
			
			System.out.println("res"+executeQuery.getString(1));
			
			
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// try {
		//
		//// session = HibernateUtil.getSessionFactory().openSession();
		//// tx = session.beginTransaction();
		//// result = (List<Compte>)
		// session.createCriteria(Compte.class).list();
		//// tx.commit();
		// } catch (HibernateException e) {
		// if (session != null && tx != null) {
		// tx.rollback();
		// }
		// throw new DataBaseAccessException(NOTHING_FOUND);
		// } finally {
		// session.close();
		// }
		return result;
	}






	/**
	 * {@inheritDoc}
	 */
	 public final boolean update(final Compte Compte)
	 throws DataBaseAccessException 
	 {
		 Session session = null;
		 boolean result = false;
		 try 
		 {
			 session = HibernateUtil.getSessionFactory().openSession();
			 session.beginTransaction();
			 session.update(Compte);
			 session.getTransaction().commit();
			 result = true;
		 } 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 result = false;
		 } 
		 finally 
		 {
			 session.close();
		 }
		 return result;
	 }

	/**
	 * {@inheritDoc}
	 */
	 public final boolean delete(final Compte Compte)
	 throws DataBaseAccessException 
	 {
		 Session session = null;
		 boolean result = false;
		 try 
		 {
			 session = HibernateUtil.getSessionFactory().openSession();
			 Transaction tx = session.beginTransaction();
			 session.delete(Compte);
			 tx.commit();
			 result = true;
		 } 
		 catch (HibernateException e) 
		 {
			 if (session != null && session.getTransaction() != null) 
			 {
				 session.getTransaction().rollback();
			 }
			 result = false;
		 } 
		 finally 
		 {
			 session.close();
		 }
		 return result;
	 }

	
}
