package fr.ausy.recrutement.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import modele.Compte;
import modele.Etape;
import modele.ParticipationEtape;
import service.EtapeServiceImpl;
import service.ParticipationEtapeServiceImpl;

@SuppressWarnings("serial")
public class EtapeAction extends ActionSupport implements SessionAware {

	protected Map<String, Object> session;

	private String erreur;
	private List<Etape> lstEtapes;
	private Etape etapeCherchee;
	private String param1;
	private ParticipationEtape participationEtapeCherchee;

	public String getErreur() {

		return erreur;
	}

	public void setErreur(String erreur) {
		this.erreur = erreur;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;

	}

	public List<Etape> getLstEtapes() {
		return lstEtapes;
	}

	public void setLstEtapes(List<Etape> lstEtapes) {
		this.lstEtapes = lstEtapes;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public Etape getEtapeCherchee() {
		return etapeCherchee;
	}

	public void setEtapeCherchee(Etape etapeCherchee) {
		this.etapeCherchee = etapeCherchee;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public ParticipationEtape getParticipationEtapeCherchee() {
		return participationEtapeCherchee;
	}

	public void setParticipationEtapeCherchee(ParticipationEtape participationEtapeCherchee) {
		this.participationEtapeCherchee = participationEtapeCherchee;
	}

	public String execute() throws Exception {
		String result = "";
		EtapeServiceImpl etapeServiceImpl = new EtapeServiceImpl();
		ParticipationEtapeServiceImpl participEtapeServiceImpl = new ParticipationEtapeServiceImpl();

		if (session.get("utilisateur") != null) {
			lstEtapes = etapeServiceImpl.findAll();
			if (param1 == null) {
				param1 = "0";
			}
			Compte candidat = (Compte) session.get("utilisateur");
			etapeCherchee = lstEtapes.get(Integer.parseInt(param1));
			// Mettre les infos de l'utilisateur, par ex : date de l'�tape
			// pr�vue ou pass�e, lieu, etc...
			participationEtapeCherchee = participEtapeServiceImpl.findParticipationEtape(candidat,etapeCherchee);
			result = "succes";
		}
		return result;
	}

}
