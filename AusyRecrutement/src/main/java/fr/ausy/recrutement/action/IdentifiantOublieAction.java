package fr.ausy.recrutement.action;

import com.opensymphony.xwork2.ActionSupport;

import modele.Compte;
import modele.Utilitaires;



@SuppressWarnings("serial")
public class IdentifiantOublieAction extends ActionSupport {
	
	private String mail;
	private String erreur;

	public String getErreur() 
	{

		return erreur;
	}

	public void setMail(String mail) 
	{
		this.mail = mail;
	}
	
	
	public String getmail() 
	{

		return mail;
	}

	public void setErreur(String erreur) 
	{
		this.erreur = erreur;
	}
	
	public String execute() throws Exception
	{
		//Etape 1 : R�cup�ration du mail
		String result = "succes";
		if(mail==null)
		{
			setErreur("Veuillez renseigner votre e-mail.");
			result="echec";
		}
		else
		{
			if(Utilitaires.VerifMail(mail)==false)
			{
				setErreur("Le mail : "+ mail + " est invalide.");
				result="echec";
			}
			else
			{
				//Chercher l'identifiant dans la base de donn�es
				Compte monCompte=new Compte();
				
				String Identifiant=monCompte.RetournerIdentifiant(mail);
				
				if(Identifiant==null)
				{
					//Si Identifiant pas trouv� :
					setErreur("Aucun compte rattach� � cette adresse mail");
					result="echec";
				}
				else
				{
					//Envoyer par mail l'identifiant trouv�
					//A finir
					String Message= "Bonjour,\n voici votre identifiant : \n" + Identifiant + "\nLe support.";
					String erreur  = Utilitaires.EnvoiMail(Message, "Candidature Ausy - Demande d'identifiant", mail);
					setErreur(erreur);
				}
				
			}
		}
		return result;
	}

}
