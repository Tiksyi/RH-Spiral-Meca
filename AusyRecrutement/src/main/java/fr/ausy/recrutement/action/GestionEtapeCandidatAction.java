package fr.ausy.recrutement.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.opensymphony.xwork2.ActionSupport;

import modele.Compte;
import modele.Etape;
import modele.ParticipationEtape;
import service.CompteServiceImpl;
import service.EtapeServiceImpl;
import service.ParticipationEtapeServiceImpl;
import utile.Constants;



@SuppressWarnings("serial")
public class GestionEtapeCandidatAction extends ActionSupport {

	
	private int choixCandidat;
	private int choixEtape;
	private String dateEtape;
	private String heureEtape;
	private String interlocuteur;
	private String lieu;
	private String erreur;
	private List<Compte> lstCandidats;
	private List<Etape> lstEtapes;

	public String getErreur() 
	{

		return erreur;
	}

	public void setErreur(String erreur) 
	{
		this.erreur = erreur;
	}

	public int getChoixCandidat() {
		return choixCandidat;
	}

	public void setChoixCandidat(int choixCandidat) {
		this.choixCandidat = choixCandidat;
	}

	public int getChoixEtape() {
		return choixEtape;
	}

	public void setChoixEtape(int choixEtape) {
		this.choixEtape = choixEtape;
	}

	public String getDateEtape() {
		return dateEtape;
	}

	public void setDateEtape(String dateEtape) {
		this.dateEtape = dateEtape;
	}

	public String getHeureEtape() {
		return heureEtape;
	}

	public void setHeureEtape(String heureEtape) {
		this.heureEtape = heureEtape;
	}

	public String getInterlocuteur() {
		return interlocuteur;
	}

	public void setInterlocuteur(String interlocuteur) {
		this.interlocuteur = interlocuteur;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	
	
	/*---------------------------------------------------------------------------------*/
	/*----------- Pour alimenter les listes d�roulantes candidats et �tapes -----------*/
	/*---------------------------------------------------------------------------------*/
	public List<Compte> getLstCandidats() 
	{
		return lstCandidats;
	}
	
	public void setLstCandidats(List<Compte> lstCandidats) 
	{
		this.lstCandidats = lstCandidats;
	}
	
	public List<Etape> getLstEtapes() 
	{
		return lstEtapes;
	}
	
	public void setLstEtapes(List<Etape> lstEtapes) 
	{
		this.lstEtapes = lstEtapes;
	}
	

	public String execute() throws Exception
	{
		
			String result = Constants.reussite;
			try
			{
				
				//Etape 0 : Remplir les listes
				//remplir la liste des candidats
				CompteServiceImpl mesCandidats=new CompteServiceImpl();
				lstCandidats=mesCandidats.findAllCandidat();
				
				//remplir la liste des �tapes
				EtapeServiceImpl mesEtapes=new EtapeServiceImpl();
				lstEtapes=mesEtapes.findAll();
				
				
				//Etape 1 : R�cup�ration des param�tres de la requ�te
				ParticipationEtape Part=new ParticipationEtape();
				if(choixCandidat!=0)
				{
					CompteServiceImpl InterrBaseDonnes =new CompteServiceImpl();
					String sIdCand=Integer.toString(choixCandidat);
					Long lIdCand=Long.parseLong(sIdCand);
					
					Compte Candidat=InterrBaseDonnes.findById(lIdCand);
					
					Part.setCandidat(Candidat);
					if(choixEtape!=0)
					{
						EtapeServiceImpl InterrBaseDonnesEt =new EtapeServiceImpl();
						String sIdEt=Integer.toString(choixEtape);
						Long lIdEt=Long.parseLong(sIdEt);
						Etape etapeChoisie=InterrBaseDonnesEt.findById(lIdEt);
					
						Part.setEtape(etapeChoisie);
						
						SimpleDateFormat sdf = new SimpleDateFormat(Constants.format_date);
						Date dateEt = sdf.parse(dateEtape);
						
						Part.setDateEtape(new java.sql.Date(dateEt.getTime()));
						
						SimpleDateFormat shf = new SimpleDateFormat(Constants.format_heure, Locale.FRANCE);
						//Enlever la date � l'heureEtape
						if(heureEtape.indexOf("T")!=-1)
						{
							heureEtape=heureEtape.substring(heureEtape.indexOf("T")+1, heureEtape.length());
						}
						Date heureEt = shf.parse(heureEtape);
						
						Part.setHeureEtape(new java.sql.Time(heureEt.getTime()));
						Part.setInterlocuteur(interlocuteur);

						Part.setLieuEtape(lieu);
						
						ParticipationEtapeServiceImpl creerBaseDonnees=new ParticipationEtapeServiceImpl();
//						try {
							Boolean bAjout=creerBaseDonnees.create(Part);
							if(bAjout.equals(false))
							{
								setErreur(Constants.erreur_ajout_BD +Constants.erreur_ajout_candidat_etape_pas + Constants.erreur_etape_candidat_exist);
								result=Constants.echec;
							}
//						} catch (Exception e) {
//							// TODO: handle exception
//						}
						
					}					
				}			

			}
			catch(Exception ex)
			{
				setErreur(Constants.erreur_general + " : "+ ex.getMessage() );
			}
		
	
		return result;
	}

	

}
