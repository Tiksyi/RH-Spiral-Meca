package fr.ausy.recrutement.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import modele.Etape;
import service.EtapeServiceImpl;
import utile.Constants;

@SuppressWarnings("serial")
public class GestionEtapeAction extends ActionSupport implements SessionAware {

	protected Map<String, Object> session;

	private String erreur;
	private List<Etape> lstEtapes;
	private Long choixEtape;
	private String nom;
	private String description;
	private String type;
	private String typeInterlocuteur;

	public String getErreur() {

		return erreur;
	}

	public void setErreur(String erreur) {
		this.erreur = erreur;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;

	}

	public List<Etape> getLstEtapes() {
		return lstEtapes;
	}

	public void setLstEtapes(List<Etape> lstEtapes) {
		this.lstEtapes = lstEtapes;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public Long getChoixEtape() {
		return choixEtape;
	}

	public void setChoixEtape(Long choixEtape) {
		this.choixEtape = choixEtape;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTypeInterlocuteur() {
		return typeInterlocuteur;
	}

	public void setTypeInterlocuteur(String typeInterlocuteur) {
		this.typeInterlocuteur = typeInterlocuteur;
	}

	public String afficher() throws Exception {
		String result = "";
		// remplir la liste des �tapes
		EtapeServiceImpl mesEtapes = new EtapeServiceImpl();
		lstEtapes = mesEtapes.findAll();
		Etape etapeChoisie = new Etape();
		if (choixEtape != null) {
			if (choixEtape != 0) {
				etapeChoisie = etapeChoisie.TrouverEtapeDansListeById(lstEtapes, choixEtape);
				result = "succes";
				nom = etapeChoisie.getNomEtape();
				description = etapeChoisie.getDescriptionEtape();
				type = etapeChoisie.getTypeEtape();
				typeInterlocuteur = etapeChoisie.getTypeInterlocuteur();
			} else {
				setErreur(Constants.choix_etape);
				result = Constants.echec;
			}
		} else {
			result = Constants.reussite;
		}
		return result;
	}

	public String execute() throws Exception {

		String result = "";
		EtapeServiceImpl mesEtapes = new EtapeServiceImpl();
		lstEtapes = mesEtapes.findAll();
		if (choixEtape != null) {
			if (choixEtape != 0) {
				if (!nom.equals("") && !type.equals("") && !typeInterlocuteur.equals("")) {
					Etape etapeChoisie = new Etape();
					etapeChoisie = etapeChoisie.TrouverEtapeDansListeById(lstEtapes, choixEtape);
					etapeChoisie.setNomEtape(nom);
					etapeChoisie.setDescriptionEtape(description);
					etapeChoisie.setTypeEtape(type);
					etapeChoisie.setTypeInterlocuteur(typeInterlocuteur);

					// remplir la liste des �tapes
					Boolean bModifie = mesEtapes.update(etapeChoisie);
					if (bModifie.equals(true)) {
						result = Constants.reussite;
					} else {
						result = Constants.echec;
					}
				}
				else
				{
					setErreur(Constants.champ_obligatoire_manquant);
					result = Constants.echec;
				}

			} else {
				setErreur(Constants.choix_etape);
				result = Constants.echec;
			}
		} else {
			result = Constants.reussite;
		}

		return result;
	}

}
