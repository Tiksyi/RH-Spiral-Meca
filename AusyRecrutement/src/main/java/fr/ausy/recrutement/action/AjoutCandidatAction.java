package fr.ausy.recrutement.action;

import org.apache.commons.lang3.StringUtils;

import com.opensymphony.xwork2.ActionSupport;

import modele.Compte;
import modele.Statut;
import modele.Utilitaires;
import service.CompteServiceImpl;
import utile.Constants;

@SuppressWarnings("serial")
public class AjoutCandidatAction extends ActionSupport {

	private String civilite;
	private String nom;
	private String prenom;
	private String mail;
	private String erreur;
	private String identifiants;
	private Boolean admin;

	public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public String getErreur() {

		return erreur;
	}

	public String getIdentifiants() {
		return identifiants;
	}

	public void setIdentifiants(String identifiants) {
		this.identifiants = identifiants;
	}

	public void setErreur(String erreur) {
		this.erreur = erreur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCivilite() {
		return civilite;
	}

	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String execute() throws Exception {
		String result = "succes";
		try {
			if (nom != null) {
				if(!nom.equals("") && !prenom.equals("")  && !mail.equals(""))
				{
					// Etape 1 : R�cup�ration des param�tres de la requ�te
					Compte Candidat = new Compte();
					Candidat.setCivilite(civilite);
					Candidat.setNom(nom);
					Candidat.setPrenom(prenom);
					Candidat.setEmail(mail);

					// V�rification de la validit� du mail
					if (Utilitaires.VerifMail(Candidat.getEmail()).equals(false)) {
						setErreur(Constants.email + " " + Candidat.getEmail() + " " + Constants.erreur_invalide);
						result = "echec";
					} else {
						Candidat.setLogin(Utilitaires.GenerateLogin(Candidat.getNom(), Candidat.getPrenom()));
						Candidat.setMdp(Utilitaires.genererMotDePasseAleatoire(10));
						Statut monStatut;
						if (admin.equals(false)) {
							// Statut candidat
							monStatut = new Statut(2, "Candidat");
							monStatut.setIdStatut(2);
							Candidat.setStatut(monStatut);
						} else {
							// Statut admin
							monStatut = new Statut(1, "Administrateur");
							monStatut.setIdStatut(1);
							Candidat.setStatut(monStatut);
						}

						// Cr�ation du candidat
						CompteServiceImpl InterrBaseDonnees = new CompteServiceImpl();

						// try {
						Boolean bAjout = InterrBaseDonnees.create(Candidat);
						if (bAjout.equals(false)) {
							setErreur(Constants.erreur_ajout_BD + Constants.erreur_ajout_candidat_pas);
						} else {
							// Envoi du mail
							String Message = Constants.email_text_ajout_candidat + " ";
							Message += Candidat.getLogin() + "\n";
							Message += Candidat.getMdp() + Constants.email_signature;
							// A finir l'envoi de mail
							String erreur = Utilitaires.EnvoiMail(Message, Constants.email_object_ajout_candidat,
									Candidat.getEmail());
							if (StringUtils.isBlank(erreur)) {
								erreur = "aucune";
							}
							setErreur(erreur);
							setIdentifiants(Constants.identifiants_ajout_candidat_login + " " + Candidat.getLogin() + " "
									+ Constants.identifiants_ajout_candidat_mdp + Candidat.getMdp());
						}

						// } catch (Exception e) {
						// // TODO: handle exception
						// }

					}
				} else {
					setErreur(Constants.champ_obligatoire_manquant);
				}

			}
		} catch (Exception ex) {
			setErreur(Constants.erreur_general + " : " + ex.getCause());
		}
		return result;
	}

}
