package fr.ausy.recrutement.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class DeconnexionAction extends ActionSupport implements SessionAware{
	
	public static final String KEY_UTILISATEUR="utilisateur";
	protected Map<String, Object> session;
	private String erreur;

	public String getErreur() 
	{

		return erreur;
	}

	public void setErreur(String erreur) 
	{
		this.erreur = erreur;
	}
	
	public void setSession(Map<String, Object> session) {
		this.session=session;
		
	}
	
	public Map<String, Object>  getSession() {
		return session;
	}
	
	public String execute() throws Exception
	{

		try
		{
			session=ActionContext.getContext().getSession();

			if(session.containsKey(KEY_UTILISATEUR)) 
			{
			 	session.put("utilisateur", null);
			 	session.remove(KEY_UTILISATEUR);
			} 

		}
		catch(Exception e)
		{
	  		String strError=e.getMessage();
			System.out.println("Error is: " + strError);
	  	}


	  	return "succes";
	}

}
