package fr.ausy.recrutement.action;

import com.opensymphony.xwork2.ActionSupport;

import modele.Compte;
import modele.Utilitaires;

@SuppressWarnings("serial")
public class MotDePasseOublieAction extends ActionSupport {

	private String mail;
	private String erreur;

	public String getErreur() 
	{

		return erreur;
	}

	public void setErreur(String erreur) 
	{
		this.erreur = erreur;
	}
	
	public void setMail(String mail) 
	{
		this.mail = mail;
	}
	
	
	public String getmail() 
	{

		return mail;
	}
	
	public String execute() throws Exception
	{

		//Etape 1 : R�cup�ration du mail
		
		String result = "succes";
		if(mail==null)
		{
			setErreur("Veuillez renseigner votre e-mail.");
			result="echec";
		}
		else
		{
			if(Utilitaires.VerifMail(mail)==false)
			{
				setErreur("Le mail : "+mail + " est invalide.");
				result="echec";
			}
			else
			{
				//Changer le mot de passe dans la base de donn�es
				Compte monCompte=new Compte();
				
				String mdp=monCompte.ChangerMdp(mail);
				
				if(mdp==null)
				{
					//Si mdp null :
					setErreur("Aucun compte rattach� � cette adresse mail");
				}
				else
				{			
					//Envoyer par mail le nouveau mot de passe 
					//A finir
					String Message= "Bonjour,\n vous avez demand� a r�initialiser votre mot de passe.\nVoici votre nouveau mot de passe : \n" + mdp + "\nLe support.";
					String erreur  = Utilitaires.EnvoiMail(Message, "Candidature Ausy - Demande de r�initialisation de mot de passe", mail);
					setErreur(erreur);
				}
			}
		}
		return result;
	}

}
