package fr.ausy.recrutement.action;

import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import modele.Compte;
import modele.Etape;
import service.CompteServiceImpl;
import service.EtapeServiceImpl;

@SuppressWarnings("serial")
public class AccueilAction extends ActionSupport implements SessionAware {

	protected Map<String, Object> session;

	String login;
	String mdp;
	private List<Etape> lstEtapes;

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *            the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the mdp
	 */
	public String getMdp() {
		return mdp;
	}

	/**
	 * @param mdp
	 *            the mdp to set
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	private String erreur;

	public String getErreur() {

		return erreur;
	}

	public void setErreur(String erreur) {
		this.erreur = erreur;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;

	}

	public Map<String, Object> getSession() {
		return session;
	}

	public List<Etape> getLstEtapes() {
		return lstEtapes;
	}

	public void setLstEtapes(List<Etape> lstEtapes) {
		this.lstEtapes = lstEtapes;
	}

	public String execute() throws Exception {
		String result = "";
		CompteServiceImpl compteServiceImpl = new CompteServiceImpl();
		
		EtapeServiceImpl mesEtapes = new EtapeServiceImpl();
		lstEtapes = mesEtapes.findAll();
		
		if (session.get("utilisateur") != null) {
			result = "succes";
		} else {

			Compte MonCompteTrouve = compteServiceImpl.findByLoginPwd(login, mdp);
			
			
			if (MonCompteTrouve != null) {
				result = "succes";
				session.put("utilisateur", MonCompteTrouve);
		

			} else {
				setErreur("Votre identifiant ou votre mot de passe est incorrect");
				result = "echec";
			}
		}
		return result;
	}

}
