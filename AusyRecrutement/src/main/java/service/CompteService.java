package service;

import java.util.List;

import core.DataBaseAccessException;
import modele.Compte;

public interface CompteService {
	/**
	 * a method that creates a Compte in Compte.
	 * @param Compte the Compte to create
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean create(Compte Compte) throws DataBaseAccessException;
	/**
	 * a method that finds a Compte.
	 * @param id the technical id of the searched Compte
	 * @return the searched Compte
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	Compte findById(final Long id) throws DataBaseAccessException;
	/**
	 * a method that finds a Compte.
	 * @param id the technical id of the searched Compte
	 * @return the searched Compte
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	Compte findByLoginPwd(final String login, String pwd) throws DataBaseAccessException;
	/**
	 * a method that returns a Compte list.
	 * @return myComptes a list of Comptes
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	
	List<Compte> findAll() throws DataBaseAccessException;
	/**
	 * a method that updates a Compte.
	 * @param Compte my Compte
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean update(Compte Compte) throws DataBaseAccessException;
	/**
	 * a method that deletes a Compte.
	 * @param Compte the Compte to delete
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean delete(Compte Compte) throws DataBaseAccessException;
	
	/**
	 * a method that finds a Compte.
	 * @param id the technical id of the searched Compte
	 * @return the searched Compte
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	Compte findByMail(String email) throws DataBaseAccessException;
	
	/**
	 * a method that returns a Compte list.
	 * @return myComptes a list of Comptes
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	
	List<Compte> findAllCandidat() throws DataBaseAccessException;
}
