package service;

import java.util.List;

import core.DataBaseAccessException;
import dao.EtapeDao;
import dao.EtapeDaoImpl;
import modele.Etape;


public class EtapeServiceImpl implements EtapeService {
	/**
	 * an EtapeDao used in all my methods.
	 */
	
	private EtapeDao etapeDao= new EtapeDaoImpl();
	/**
	 *{@inheritDoc}
	 */
	
	public final boolean create(Etape etape)
			throws DataBaseAccessException {
		return etapeDao.create(etape);
	}

	/**
	 * {@inheritDoc}
	 */
	
	public final Etape findById(final Long id)
			throws DataBaseAccessException {
		return etapeDao.findById(id);
	}
	
	/**
	 * {@inheritDoc}
	 */
	
	public final List<Etape> findAll()
			throws DataBaseAccessException {
		return etapeDao.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	
	public final boolean update(final Etape etape)
			throws DataBaseAccessException {
		return etapeDao.update(etape);

	}

	/**
	 * {@inheritDoc}
	 */
	
	public final boolean delete(final Etape etape)
			throws DataBaseAccessException {
		 return etapeDao.delete(etape);
	}

	
	
}
