package service;

import java.util.List;

import core.DataBaseAccessException;
import dao.StatutDao;
import dao.StatutDaoImpl;
import modele.Statut;


public class StatutServiceImpl implements StatutService {
	/**
	 * an StatutDao used in all my methods.
	 */
	
	private StatutDao statutDao= new StatutDaoImpl();
	

	/**
	 * {@inheritDoc}
	 */
	
	public final Statut findById(final Long id)
			throws DataBaseAccessException {
		return statutDao.findById(id);
	}
	
	/**
	 * {@inheritDoc}
	 */
	
	public final List<Statut> findAll()
			throws DataBaseAccessException {
		return statutDao.findAll();
	}

	
}
