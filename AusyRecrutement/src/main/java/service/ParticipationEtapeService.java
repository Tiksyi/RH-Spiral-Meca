package service;

import java.util.List;

import core.DataBaseAccessException;
import modele.Compte;
import modele.Etape;
import modele.ParticipationEtape;

public interface ParticipationEtapeService {
	/**
	 * a method that creates a ParticipationEtape in ParticipationEtape.
	 * @param ParticipationEtape the ParticipationEtape to create
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean create(ParticipationEtape ParticipationEtape) throws DataBaseAccessException;
	
	/**
	 * a method that returns a ParticipationEtape list.
	 * @return myParticipationEtapes a list of ParticipationEtape
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	
	List<ParticipationEtape> findAll(Compte candidat) throws DataBaseAccessException;
	
	/**
	 * a method that find a ParticipationEtape.
	 * @param Compte my candidat, Etape my etape
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	ParticipationEtape findParticipationEtape(Compte candidat, Etape etape) throws DataBaseAccessException;
	
	/**
	 * a method that updates a ParticipationEtape.
	 * @param ParticipationEtape my ParticipationEtape
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean update(ParticipationEtape ParticipationEtape) throws DataBaseAccessException;
	/**
	 * a method that deletes a ParticipationEtape.
	 * @param ParticipationEtape the ParticipationEtape to delete
	 * @throws DataBaseAccessException a message error if no access to database
	 * @return true if the method is a success
	 */
	boolean delete(ParticipationEtape ParticipationEtape) throws DataBaseAccessException;

}
