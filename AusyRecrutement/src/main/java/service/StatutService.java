package service;

import java.util.List;

import core.DataBaseAccessException;
import modele.Statut;

public interface StatutService {
	/**
	
	/**
	 * a method that finds a Statut.
	 * @param id the technical id of the searched Statut
	 * @return the searched Statut
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	Statut findById(final Long id) throws DataBaseAccessException;
	
	/**
	 * a method that returns a Statut list.
	 * @return myComptes a list of Statut
	 * @throws DataBaseAccessException a message error if no access to database
	 */
	
	List<Statut> findAll() throws DataBaseAccessException;
}
