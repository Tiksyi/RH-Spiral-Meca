package service;

import java.util.List;

import core.DataBaseAccessException;
import dao.ParticipationEtapeDao;
import dao.ParticipationEtapeDaoImpl;
import modele.Compte;
import modele.Etape;
import modele.ParticipationEtape;


public class ParticipationEtapeServiceImpl implements ParticipationEtapeService {
	/**
	 * an ParticipationEtapeDao used in all my methods.
	 */
	
	private ParticipationEtapeDao participationEtapeDao= new ParticipationEtapeDaoImpl();
	/**
	 *{@inheritDoc}
	 */
	
	public final boolean create(ParticipationEtape Part)
			throws DataBaseAccessException {
		return participationEtapeDao.create(Part);
	}

	
	/**
	 * {@inheritDoc}
	 */
	
	public final List<ParticipationEtape> findAll(Compte candidat)
			throws DataBaseAccessException {
		return participationEtapeDao.findAll(candidat);
	}

	/**
	 * {@inheritDoc}
	 */
	
	public final boolean update(final ParticipationEtape ParticipationEtape)
			throws DataBaseAccessException {
		return participationEtapeDao.update(ParticipationEtape);

	}
	
	/**
	 * {@inheritDoc}
	 */
	public final ParticipationEtape findParticipationEtape(Compte candidat,Etape etape)
			throws DataBaseAccessException {
		return participationEtapeDao.findParticipationEtape(candidat, etape);
	}

	/**
	 * {@inheritDoc}
	 */
	
	public final boolean delete(final ParticipationEtape part)
			throws DataBaseAccessException {
		 return participationEtapeDao.delete(part);
	}

	
	

}
