package service;

import java.util.List;

import core.DataBaseAccessException;
import dao.CompteDao;
import dao.CompteDaoImpl;
import modele.Compte;


public class CompteServiceImpl implements CompteService {
	/**
	 * an CompteDao used in all my methods.
	 */
	
	private CompteDao compteDao= new CompteDaoImpl();
	/**
	 *{@inheritDoc}
	 */
	
	public final boolean create(Compte Compte)
			throws DataBaseAccessException {
		return compteDao.create(Compte);
	}

	/**
	 * {@inheritDoc}
	 */
	
	public final Compte findById(final Long id)
			throws DataBaseAccessException {
		return compteDao.findById(id);
	}
	/**
	 * {@inheritDoc}
	 */
	
	public final Compte findByLoginPwd(final String login, final String pwd)
			throws DataBaseAccessException {
		return compteDao.findByLoginPwd(login, pwd);
	}
	/**
	 * {@inheritDoc}
	 */
	
	public final List<Compte> findAll()
			throws DataBaseAccessException {
		return compteDao.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	
	public final boolean update(final Compte Compte)
			throws DataBaseAccessException {
		return compteDao.update(Compte);

	}

	/**
	 * {@inheritDoc}
	 */
	
	public final boolean delete(final Compte Compte)
			throws DataBaseAccessException {
		 return compteDao.delete(Compte);
	}

	
	public final Compte findByMail(String email) throws DataBaseAccessException {
		 return compteDao.findByMail(email);
	}
	
	public final List<Compte> findAllCandidat() throws DataBaseAccessException
	{
			return compteDao.findAllCandidat();
	}

	

}
