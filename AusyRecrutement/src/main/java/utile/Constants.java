package utile;

public interface Constants {
	
	
	public static final String email_support = "support.recrutement@ausy.fr";
	public static final String email_text_ajout_candidat="Bienvenue dans le parcours candidat d'Ausy.\nPour vous connecter, voici votre login et votre mot de passe :\n";
	public static final String email_object_ajout_candidat="Bienvenue";
	public static final String identifiants_ajout_candidat_login="Voici le login : ";
	public static final String identifiants_ajout_candidat_mdp="et le mot de passe : ";
	public static final String email_signature="\nLe support.";
	public static final String email="Le mail";
	public static final String email_en_developpement="La fonctionnalit� d'envoi de mail est en cours de d�veloppement";
	
	public static final String statut_admin="Administrateur";
	
	public static final String echec="echec";
	public static final String reussite="succes";
	
	public static final String erreur="erreur";
	public static final String erreur_invalide=" est invalide.";
	public static final String erreur_general="Une erreur est survenue";
	public static final String erreur_ajout_BD="Une erreur est survenue lors de l'ajout a la base de donn�es.\n";
	public static final String erreur_ajout_candidat_pas = "Le candidat n'a pu �tre ajout�.";
	public static final String erreur_ajout_candidat_etape_pas = "L'�tape n'a pas pu �tre rajout�e au candidat. ";
	public static final String erreur_trouve_etape="Probl�me lors de la r�cup�ration de l'�tape";
	public static final String erreur_trouve_candidat="Probl�me lors de la r�cup�ration du candidat";
	
	public static final String erreur_etape_candidat_exist="V�rifiez si l'�tape n'existe pas d�j� pour ce candidat";
	
	public static final String format_heure= "hh:mm:ss";
	public static final String format_date= "yyyy-MM-dd";
	
	public static final String champ_obligatoire_manquant="Au moins une donn�e obligatoire est manquante";
	public static final String choix_etape="Veuillez choisir une �tape � modifier";
}
