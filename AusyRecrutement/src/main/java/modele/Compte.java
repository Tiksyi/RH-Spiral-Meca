package modele;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import core.DataBaseAccessException;
import service.CompteServiceImpl;

@Entity
@Table(name = "utilisateur")
public class Compte {

	/**
	 * id
	 */

	private Long id;

	/**
	 * login
	 */

	private String login;

	/**
	 * pwd
	 */
	private String mdp;

	/**
	 * email
	 */
	private String email;

	/**
	 * civilite
	 */
	private String civilite;

	/**
	 * nom
	 */
	private String nom;

	/**
	 * prenom
	 */
	private String prenom;

	/**
	 * statut
	 */
	private Statut statut;

	/**
	 * @return the login
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "login")
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *            the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the pwd
	 */
	@Column(name = "mdp")
	public String getMdp() {
		return mdp;
	}

	/**
	 * @param pwd
	 *            the pwd to set
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the civility
	 */
	public String getCivilite() {
		return civilite;
	}

	/**
	 * @param civilite
	 *            the civility to set
	 */
	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom
	 *            the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom
	 *            the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the statut
	 */
	@ManyToOne(targetEntity = Statut.class, cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "idStatut")
	public Statut getStatut() {
		return statut;
	}

	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	public Compte(String login, String mdp) {
		super();
		this.login = login;
		this.mdp = mdp;
	}

	public Compte() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the listRealisation
	 * 
	 * @ManyToMany(mappedBy = "listCompte") public List<Realisation>
	 *                      getListRealisation() { return listRealisation; }
	 */
	/**
	 * @param listRealisation
	 *            the listRealisation to set
	 * 
	 *            public void setListRealisation(List<Realisation>
	 *            listRealisation) { this.listRealisation = listRealisation; }
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Compte";
	}

	// V�rifier si l'email existe dans la base de donn�es
	public String RetournerIdentifiant(String email) throws DataBaseAccessException {
		String login = null;
		CompteServiceImpl InterrBaseDonnees = new CompteServiceImpl();
		Compte monCompteTrouve = InterrBaseDonnees.findByMail(email);
		if (monCompteTrouve != null) {
			login = monCompteTrouve.getMdp();
		}
		return login;
	}

	// Trouve le compte associ�, change le mot de passe et le retourne
	public String ChangerMdp(String email) throws DataBaseAccessException {
		String mdp = null;
		CompteServiceImpl InterrBaseDonnees = new CompteServiceImpl();
		Compte monCompteTrouve = InterrBaseDonnees.findByMail(email);

		if (monCompteTrouve != null) {
			mdp = Utilitaires.genererMotDePasseAleatoire(10);
			monCompteTrouve.setMdp(mdp);
			InterrBaseDonnees.update(monCompteTrouve);
		}
		return mdp;
	}
	

	

}
