package modele;

import java.io.Serializable;

import javax.persistence.Embeddable;

@SuppressWarnings("serial")
@Embeddable
public class ParticipationEtapePK implements Serializable {
	

	
	private Compte candidat;
	private Etape etape;
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((candidat == null) ? 0 : candidat.hashCode());
		result = prime * result + ((etape == null) ? 0 : etape.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticipationEtapePK other = (ParticipationEtapePK) obj;
		if (candidat == null) {
			if (other.candidat != null)
				return false;
		} else if (!candidat.equals(other.candidat))
			return false;
		if (etape == null) {
			if (other.etape != null)
				return false;
		} else if (!etape.equals(other.etape))
			return false;
		return true;
	}
	/**
	 * @return the candidat
	 */
	public Compte getCandidat() {
		return candidat;
	}
	/**
	 * @param candidat the candidat to set
	 */
	public void setCandidat(Compte candidat) {
		this.candidat = candidat;
	}
	/**
	 * @return the etape
	 */
	public Etape getEtape() {
		return etape;
	}
	/**
	 * @param etape the etape to set
	 */
	public void setEtape(Etape etape) {
		this.etape = etape;
	}
	
	



	

	
	
	
	
	
	
	
		

}
