package modele;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "statut")
public class Statut  {

	/**
	 * idStatut
	 */
	
	private int idStatut;
	
	/**
	 * Statut
	 */
	private String statut;
	
	
	@Id
	@GeneratedValue
	@Column(name = "idStatut")
	public int getIdStatut() {
		return idStatut;
	}
	
	/**
	 * @param idStatut
	 *            the idStatut to set
	 */
	public void setIdStatut(int idStatut) {
		this.idStatut = idStatut;
	}
	/**
	 * @return the statut
	 */
	@Column(name = "statut")
	public String getStatut() {
		return statut;
	}
	
	/**
	 * @param statut
	 *            the statut to set
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}

	
	public Statut(int idStatut, String statut)
	{
		super();
		this.idStatut=idStatut;
		this.statut=statut;
	}
	
	public Statut()
	{
		super();
	}

}
