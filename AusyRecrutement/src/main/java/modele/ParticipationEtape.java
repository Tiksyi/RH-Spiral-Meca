package modele;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(ParticipationEtapePK.class)
@Table(name = "participationetape")


public class ParticipationEtape {	
	
	/**
	 * candidat
	 */
	@Id
	@ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Compte candidat;
	
	/**
	 * etape
	 */
	
	@Id
    @ManyToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private Etape etape;
	
	
	
	/**
	 * dateEtape
	 */
	private Date dateEtape;


	/**
	 * heureEtape
	 */
	private Time heureEtape;
	
	
	/**
	 * lieuEtape
	 */
	private String lieuEtape;
	
	
	/**
	 * interlocuteur
	 */
	private String interlocuteur;
	
	
	/**
	 * @return the candidat
	*/
	@ManyToOne(targetEntity = Compte.class, cascade={CascadeType.PERSIST})
	@JoinColumn(name="id")
	@Id
	public Compte getCandidat() {
		return candidat;
	} 

	/**
	 * @param candidat the candidat to set
	 */
	public void setCandidat(Compte candidat) {
		this.candidat = candidat;
	}
	
	/**
	 * @return the etape
	*/
	@ManyToOne(targetEntity = Etape.class, cascade={CascadeType.PERSIST})
	@JoinColumn(name="idEtape")
	@Id
	public Etape getEtape() {
		return etape;
	} 

	/**
	 * @param etape the etape to set
	 */
	public void setEtape(Etape etape) {
		this.etape = etape;
	}
	
	
	@Column(name = "dateEtape")
	public Date getDateEtape() {
		return dateEtape;
	}

	/**
	 * @param dateEtape
	 *            the dateEtape to set
	 */
	public void setDateEtape(Date dateEtape) {
		this.dateEtape = dateEtape;
	}

	/**
	 * @return the heureEtape
	 */
	@Column(name = "heureEtape")
	public Time getHeureEtape() {
		return heureEtape;
	}

	/**
	 * @param heureEtape
	 *            the heureEtape to set
	 */
	public void setHeureEtape(Time heureEtape) {
		this.heureEtape = heureEtape;
	}

	
	/**
	 * @return the lieuEtape
	 */
	public String getLieuEtape() {
		return lieuEtape;
	}

	/**
	 * @param lieuEtape the lieuEtape to set
	 */
	public void setLieuEtape(String lieuEtape) {
		this.lieuEtape = lieuEtape;
	}
	
	/**
	 * @return the interlocuteur
	 */
	public String getInterlocuteur() {
		return interlocuteur;
	}
	
	/**
	 * @param interlocuteur the interlocuteur to set
	 */
	public void setInterlocuteur(String interlocuteur) {
		this.interlocuteur = interlocuteur;
	}



	public ParticipationEtape()
	{
		super();

	}


	

	@Override
	public String toString() {
		return "Participation";
	}
	
	
	/*//V�rifier si le couple �tape candidat existe dans la base de donn�es
	public int RetournerNbParticipationEtape(Compte candidat, Etape etape) throws DataBaseAccessException
	{
		int nbParticipation=0;
		ParticipationEtapeServiceImpl InterrBaseDonnees=new ParticipationEtapeServiceImpl();
		//nbParticipation =InterrBaseDonnees.findNbParticipation(candidat.getId(), etape.getIdEtape());
		
		return nbParticipation;
	}
	
	
	*/
	
	
		

}
