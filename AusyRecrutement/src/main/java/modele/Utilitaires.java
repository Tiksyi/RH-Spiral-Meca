package modele;


import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.RandomStringUtils;

import core.DataBaseAccessException;
import utile.Constants;




public class Utilitaires 
{
	/**
	 * V�rifier la validit� d'un mail
	 */
	public static Boolean VerifMail(String email) {
		Boolean bValide=false;
		Pattern patTest=Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$");
		Matcher matchTest=patTest.matcher(email.toUpperCase());
		bValide=matchTest.matches();
		return bValide;
	}
	
	//G�n�ration de mot de passe al�atoire
	public static String genererMotDePasseAleatoire(int length)
	{
		return RandomStringUtils.randomAlphanumeric(length);
	}

	//Envoi d'un mail
	public static String EnvoiMail(String Message, String objet, String destinataire)
	{
		String BienPasse=null;
		
		Properties props=new Properties();
		String nom_du_serveur_smtp="srv-tlse-tech.ausytech.ausy.fr";
		props.put("mail.smtp.host", nom_du_serveur_smtp);
		
		// Cr�er l�objet Session.
		Session session = Session.getDefaultInstance(props, null);
		session.setDebug(true); //activer le mode verbeux !
		
		try
		{
			//Cr�ation
			Message mess=new MimeMessage(session);
			
			//Envoy� par
			mess.setFrom(new InternetAddress(Constants.email_support));
			//Destinataire
			InternetAddress toAdress=new InternetAddress(destinataire);
			mess.addRecipient(RecipientType.TO, toAdress);
			
			//Objet
			mess.setSubject(objet);
			
			//Corps du message
			mess.setText(Message);
			
			//Envoi
			Transport.send(mess);
		}
		catch (MessagingException ex) 
		{
			//BienPasse=ex.getMessage();
			BienPasse=Constants.email_en_developpement;
		}
		return BienPasse;
	}
	 
	//G�n�rer le login (1ere lettre du prenom + nom sans espace en minuscule)
	public static String GenerateLogin(String nom, String prenom) throws DataBaseAccessException
	{
		String loginGenere=prenom.charAt(0) + nom.replaceAll(" ", "");
		return loginGenere.toLowerCase();
	}
}
