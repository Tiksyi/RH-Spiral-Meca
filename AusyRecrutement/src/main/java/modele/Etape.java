package modele;

import java.util.Iterator;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import core.DataBaseAccessException;

@Entity
@Table(name = "etape")
public class Etape {

	/**
	 * idEtape
	 */

	private Long idEtape;

	/**
	 * nomEtape
	 */

	private String nomEtape;

	/**
	 * typeEtape
	 */
	private String typeEtape;

	/**
	 * descriptionEtape
	 */

	private String descriptionEtape;

	/**
	 * typeInterlocuteur
	 */

	private String typeInterlocuteur;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idEtape", unique = true, nullable = false)
	public Long getIdEtape() {
		return idEtape;
	}

	/**
	 * @param idEtape
	 *            the idEtape to set
	 */
	public void setIdEtape(Long idEtape) {
		this.idEtape = idEtape;
	}

	@Column(name = "nomEtape")
	public String getNomEtape() {
		return nomEtape;
	}

	/**
	 * @param descriptionEtape
	 *            the descriptionEtape to set
	 */
	public void setNomEtape(String nomEtape) {
		this.nomEtape = nomEtape;
	}

	/**
	 * @return the typeEtape
	 */
	@Column(name = "typeEtape")
	public String getTypeEtape() {
		return typeEtape;
	}

	/**
	 * @param typeEtape
	 *            the typeEtape to set
	 */
	public void setTypeEtape(String typeEtape) {
		this.typeEtape = typeEtape;
	}

	@Column(name = "descriptionEtape")
	public String getDescriptionEtape() {
		return descriptionEtape;
	}

	/**
	 * @param descriptionEtape
	 *            the descriptionEtape to set
	 */
	public void setDescriptionEtape(String descriptionEtape) {
		this.descriptionEtape = descriptionEtape;
	}

	@Column(name = "typeInterlocuteur")
	public String getTypeInterlocuteur() {
		return typeInterlocuteur;
	}

	/**
	 * @param typeInterlocuteur
	 *            the typeInterlocuteur to set
	 */
	public void setTypeInterlocuteur(String typeInterlocuteur) {
		this.typeInterlocuteur = typeInterlocuteur;
	}

	public Etape() {
		super();
	}

	// V�rifier si l'email existe dans la base de donn�es
	public Etape TrouverEtapeDansListeById(List<Etape> lstEtapes, Long idEtape) throws DataBaseAccessException {
		Boolean bTrouve = false;
		Etape etapeChoisie = null;
		if (lstEtapes != null) {
			Iterator<Etape> parcoursListe = lstEtapes.iterator();

			while (parcoursListe.hasNext() && bTrouve == false) {
				Etape etapeEnCours = (Etape) parcoursListe.next();
				if (etapeEnCours.getIdEtape().longValue() == idEtape) {
					etapeChoisie = etapeEnCours;
					bTrouve = true;
				}
			}
		}
		return etapeChoisie;
	}

}
